// Make sure we have set and can read required environment variable(s)
(function () {
    if (!process.env.CHECKFORTEAMUPDATES_WEBHOOK) {
        throw new Error("checkForTeamUpdates error: CHECKFORTEAMUPDATES_WEBHOOK start-up environment variable is not set!");
    }
})();
var webHookUrl = process.env.CHECKFORTEAMUPDATES_WEBHOOK; // Webhook URL and key from an environment variable https://hooks.slack.com/services/SomeSecretKeyGoesHere

var fs = require("fs");
var checkForTeamUpdates = require("frc-team-updates-slack-notifer");

var CronJob = require("cron").CronJob;

var localUpdatesPath = "./teamUpdates.json";

var options = {
    // the URI of your incoming slack webhook
    webhook: webHookUrl,

    // the local team updates, this should start off as a JSON object with 
    // a team_updates property that"s an empty array and will populate itself
    // (see ./teamUpdates.json for an example)
    localUpdates: require(localUpdatesPath)
};

var c = 0;

function saveData(err, data, hadUpdate) {
    if(err) {
        console.log("heckForTeamUpdates saveData: " +err);
    }
    
    if(hadUpdate) {
        fs.writeFile(localUpdatesPath, JSON.stringify(data), function(err) {
            if(err) {
                console.trace("checkForTeamUpdates writeFilea: " + err);
            }

            options.localUpdates = data;

            console.log("checkForTeamUpdates hadUpdate saved new data");
        });
    }
}

var execute = function() {
    checkForTeamUpdates(options, saveData);

    console.log("checkForTeamUpdates will execute again in 30 minutes");
};

return new CronJob("00 */30 * * * *", (function () {
  return execute();
}), null, true, "America/New_York");
